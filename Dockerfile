FROM jboss/wildfly:10.1.0.Final

ADD q-tester-*.war /opt/jboss/wildfly/standalone/deployments/

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0", "-c" , "standalone-full.xml"]
