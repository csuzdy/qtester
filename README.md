## Description
qtester is a web-based JMS test client. Just deploy the the `q-tester-*.war` file to an application server and browse or send JMS messages with your browser.

## Prerequisites
* Java 8
* Application server with Java EE 7 support
* A connection factory, available via JNDI lookup
* Modern browser with JavaScript support

## Features
* Use any connection factory available on the application server with JNDI lookup
* Send JMS text messages to the specified queue
* Send custom JMS headers with your message
* Browse messages on the queue, read the contents of messages syntax highlighted.
* Save and reload values of input boxes automatically

## Test it in a Docker container
```docker
docker build -t csuzdy:qtester .
docker run --rm -it -p 8080:8080 csuzdy:qtester
```
Open [http://localhost:8080/qtester/index.html](http://localhost:8080/qtester/index.html).

Connection factory: `java:/JmsXA`

Queue name: `DLQ`

## Screenshots
### Sending a message
![Sending a message](/screenshots/send.png)

### Viewing a message
![Viewing a message](/screenshots/message.png)
