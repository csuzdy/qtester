toastr.options.closeButton = true;
toastr.options.closeDuration = 100;
var table;
function onLoad() {
	$('input[type="text"],textarea').change(function(e) {
		localStorage.setItem(e.target.id, e.target.value);
	});
	$('input[type="text"],textarea').each(function(index, element) {
		var val = localStorage.getItem(element.id);
		if (val) {
			$(element).val(val);		
		}
	});
}

function initTable() {
	table = $('#messages').DataTable({
		processing: true,
		stateSave: true,
		ajax: getMessages,
		deferRender: true,
		deferLoading: 0,
		"columns": [
            { "data": "messageId" },
            { "data": "correlationId" },
            { "data": "timestamp",
              "render": function (data, type, row) {
                    return new Date(data).toISOString();
               }
            },
            { "data": "replyTo" },
            { "data": "type" }
        ]
    });
	$('#messages tbody').on('click', 'tr', onRowClick);
	$('#tableRow').show();
}

function onRowClick(e) {
    var data = table.row(this).data();
    var id = data['messageId'];
    $('#messageContent .modal-title').text('Contents of ' + id);
    $.get('getMessage?messageId=' + id)
     .done(function(data) {
    	 $('#messageContent .modal-body code')
    	  .text(data)
    	  .each(function(i, block) {
    		  hljs.highlightBlock(block);
    	   });
    	 $('#messageContent').modal('show');
      })
      .fail(handleError);
}

function sendMessage(e) {
	e.preventDefault();
	var json = $(e.target).serialize();
	console.log("send: " + json);
	
	$('#sendBtn').prop('disabled', true);
	$.post("sendMessage", json)
	 .done(function(data) {
		toastr.success('Message sent. (' + data + ')');
	 })
	 .fail(handleError)
	 .always(function() {
		$('#sendBtn').prop('disabled', false);
	 });
}

function refreshTable(e) {
	e.preventDefault();
	if (!table) {
		initTable();
	} else {
		table.ajax.reload();		
	}
}

function getMessages(data, callback, settings) {
	var params = $('#connectionFactoryJNDI,#queueName').serialize();
	console.log("send: " + params);
	
	$.get("getMessages?" + params)
	 .done(function(data) {
		 callback({data: data});
	 })
	 .fail(function(jqXHR, statusText, ex) {
		 handleError(jqXHR, statusText, ex);
		 callback({data: []});
	 });
}

function handleError(jqXHR, statusText, ex) {
	var text = statusText;
	if (jqXHR && jqXHR['responseJSON']) {
		text = jqXHR.responseJSON.message;
	}
	toastr.error('Error: ' + text);
}