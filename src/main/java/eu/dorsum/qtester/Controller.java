package eu.dorsum.qtester;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.NamingException;

import org.springframework.http.MediaType;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jndi.JndiTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	private static final boolean TEST = false;
	
	private JndiTemplate jndi = new JndiTemplate();
	private Map<String, QueueMessage> messages = new ConcurrentHashMap<>();

	@RequestMapping("/test")
    public String test() {
        return "Greetings from Spring Boot!";
    }
	
	@RequestMapping(value = "/sendMessage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String sendMessage(TesterMessage msg) {
		try {
			ConnectionFactory connectionFactory = jndi.lookup(msg.getConnectionFactoryJNDI(), ConnectionFactory.class);
			JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
			jmsTemplate.send(msg.getQueueName(), (session) -> createMessage(session, msg));
		} catch (NamingException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Unable to lookup connection factory: " + msg.getConnectionFactoryJNDI());
		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Unable to send message: " + e.getMessage());
		}
        return "OK";
    }
	
	@RequestMapping(value = "/getMessages", method = RequestMethod.GET)
	public List<QueueMessage> getMessages(TesterMessage msg) {
		List<QueueMessage> retList = new ArrayList<>();
		try {
			if (TEST) {
				retList = mockMessages();
			} else {
				addMessagesOfQueue(msg, retList);
			}
		} catch (NamingException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Unable to lookup connection factory: " + msg.getConnectionFactoryJNDI());
		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Unable to browse messages: " + e.getMessage());
		}
		messages.clear();
		retList.forEach(elem -> {
			messages.put(elem.getMessageId(), elem);
		});
		return retList;
	}

	@RequestMapping(value = "/getMessage", method = RequestMethod.GET)
	public String getMessage(@RequestParam String messageId) {
		QueueMessage message = messages.get(messageId);
		if (message == null) {
			throw new IllegalArgumentException("Message not found");
		}
		return message.getText();
	}
	
	private void addMessagesOfQueue(TesterMessage msg, List<QueueMessage> retList) throws NamingException {
		ConnectionFactory connectionFactory = jndi.lookup(msg.getConnectionFactoryJNDI(), ConnectionFactory.class);
		JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
		jmsTemplate.browse(msg.getQueueName(), (session, browser) -> {
			Enumeration<?> messages = browser.getEnumeration();
			while (messages.hasMoreElements()) {
				Message message = (Message)messages.nextElement();
				retList.add(convert(message));
			}
			return null;
		});
	}

	private QueueMessage convert(Message message) throws JMSException {
		QueueMessage qmsg = new QueueMessage();
		qmsg.setMessageId(message.getJMSMessageID());
		qmsg.setCorrelationId(message.getJMSCorrelationID());
		Destination dest = message.getJMSReplyTo();
		if (dest != null) {
			qmsg.setReplyTo(dest.toString());
		}
		qmsg.setTimestamp(message.getJMSTimestamp());
		qmsg.setExpiration(message.getJMSExpiration());
		if (message instanceof TextMessage) {
			qmsg.setType("text");
			qmsg.setText(((TextMessage)message).getText());
		} else if (message instanceof ObjectMessage) {
			qmsg.setType("object");
		} else {
			qmsg.setType("other");
		}
		
		return qmsg;
	}
	
	private Message createMessage(Session session, TesterMessage msg) throws JMSException {
		TextMessage textMessage = session.createTextMessage(msg.getMessage());
		String headers = msg.getHeaders();
		if (StringUtils.hasText(headers)) {
			for (String pair : headers.split(",")) {
				if (StringUtils.hasText(pair) == false) {
					continue;
				}
				String[] pairArr = pair.split("=");
				if (pairArr.length == 2) {
					textMessage.setStringProperty(pairArr[0].trim(), pairArr[1].trim());
				}
			}
		}
		return textMessage;
	}
	
	private List<QueueMessage> mockMessages() throws IOException {
		List<QueueMessage> retList = new ArrayList<>();
		QueueMessage msg = new QueueMessage();
		msg.setMessageId("378fdsa8uf89232143243214321:Id_31321fds");
		msg.setCorrelationId("378fdsa8uf89232143243214321:Id_31321fds");
		msg.setExpiration(new Date().getTime());
		msg.setTimestamp(new Date().getTime());
		msg.setType("text");
		msg.setText(readInputStream("text.xml"));
		retList.add(msg);
		return retList;
	}
	
	private String readInputStream(String fileName) throws IOException {
		try (InputStream is = this.getClass().getResourceAsStream(fileName);
			 Scanner s = new Scanner(is)) {
			s.useDelimiter("\\A");
			return s.hasNext() ? s.next() : "";
		}
	}
}
