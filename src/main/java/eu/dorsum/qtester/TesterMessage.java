package eu.dorsum.qtester;

public class TesterMessage {

	private String connectionFactoryJNDI;
	private String queueName;
	private String message;
	private String headers;

	public String getConnectionFactoryJNDI() {
		return connectionFactoryJNDI;
	}

	public void setConnectionFactoryJNDI(String connectionFactoryJNDI) {
		this.connectionFactoryJNDI = connectionFactoryJNDI;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}
	
}
